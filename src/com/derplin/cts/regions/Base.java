package com.derplin.cts.regions;

import com.derplin.cts.teams.TeamManager;
import com.derplin.games.events.region.PlayerIsIn;
import com.derplin.games.regions.PlayerIsInEvented;
import com.derplin.games.regions.Region2D;
import java.util.ArrayList;
import net.minecraft.server.v1_7_R2.PacketPlayOutWorldParticles;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_7_R2.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;

public class Base extends Region2D implements PlayerIsInEvented {
    
    public final ArrayList<String> isIn = new ArrayList<>();
    
    public Base(int maxX, int maxZ, int minX, int minZ) {
        super(maxX, maxZ, minX, minZ);
        PlayerIsIn.addEvented(this);
    }
    
    @Override
    public void onPlayerIsIn(PlayerMoveEvent event) {
        Player p = event.getPlayer();
        String pn = p.getName();
        if (!isIn.contains(pn)) {
            isIn.add(pn);
        }
        Location l = p.getLocation();
        if (TeamManager.getCTSTeam(p).getBase() != this &&
                l.getY() % 1 == 0 && l.getBlock().getRelative(0, -1, 0).getType().isSolid()) {
            PacketPlayOutWorldParticles packet =
                new PacketPlayOutWorldParticles("footstep", (float) l.getX(), (float) (l.getY() + 0.025), (float) l.getZ(), 0.1F, 0.005F, 0.1F, 0, 1);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
            for (Entity e : p.getNearbyEntities(32, 32, 32)) {
                if (e instanceof Player) {
                    ((CraftPlayer) e).getHandle().playerConnection.sendPacket(packet);
                }
            }
        }
    }

    @Override
    public void onPlayerNotIn(PlayerMoveEvent event) {
        String pn = event.getPlayer().getName();
        if (isIn.contains(pn)) {
            isIn.remove(pn);
            
        }
    }
    
}