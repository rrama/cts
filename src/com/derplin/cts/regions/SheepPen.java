package com.derplin.cts.regions;

import com.derplin.cts.teams.CTSTeam;
import com.derplin.cts.teams.TeamManager;
import com.derplin.games.Games;
import com.derplin.games.regions.LivingEntityIsIn;
import com.derplin.games.regions.LivingEntityIsInEvented;
import com.derplin.games.regions.Region3D;
import com.derplin.roundedgames.Rounded;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Sheep;
import org.bukkit.util.Vector;

public class SheepPen extends Region3D implements LivingEntityIsInEvented {
    
    private final String sheepName;
    
    public SheepPen(CTSTeam team, Vector bMax, Vector bMin) {
        super(bMax, bMin);
        sheepName = team.getColouredName() + " Team's Sheep";
        LivingEntityIsIn.addEvented(this);
    }

    @Override
    public void onLivingEntityIsIn(LivingEntity entity) {
        if (entity.getCustomName() == null || !entity.getCustomName().equalsIgnoreCase(sheepName)) return;
        Bukkit.broadcastMessage(sheepName + " has been captured.");
        entity.remove();
        CTSTeam team = TeamManager.getCTSTeam(((Sheep) entity).getColor());
        for (OfflinePlayer op : team.getTeam().getPlayers()) {
            if (op.isOnline()) {
                Games.awardXP(op.getName(), 10);
            }
        }
        Rounded.callRoundEnd();
    }

    @Override
    public boolean acceptsEntityType(EntityType et) {
        return et == EntityType.SHEEP;
    }
    
}