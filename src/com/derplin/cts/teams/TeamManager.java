package com.derplin.cts.teams;

import com.derplin.teamgames.TeamGames;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.scoreboard.Team;

public class TeamManager {
    
    private static CTSTeam red, blue;
    
    public static void setupTeams() {
        red  = new CTSTeam("Red" , ChatColor.RED , DyeColor.RED );
        blue = new CTSTeam("Blue", ChatColor.BLUE, DyeColor.BLUE);
        TeamGames.splitPlayersToTeams();
    }
    
    public static CTSTeam getCTSTeam(OfflinePlayer op) {
        return getCTSTeam(TeamGames.getPlayerTeam(op));
    }
    
    public static CTSTeam getCTSTeam(Team team) {
        return getCTSTeam(team.getName());
    }
    
    public static CTSTeam getCTSTeam(ChatColor colour) {
        return getCTSTeam(colour.name());
    }
    
    public static CTSTeam getCTSTeam(DyeColor colour) {
        return getCTSTeam(colour.name());
    }
    
    public static CTSTeam getCTSTeam(String name) {
        switch (name.toLowerCase()) {
            case "red":
                return red;
            case "blue":
                return blue;
        }
        return null;
    }
    
    public static CTSTeam getRed() {
        return red;
    }
    
    public static CTSTeam getBlue() {
        return blue;
    }
    
    public static CTSTeam[] getTeams() {
        return new CTSTeam[] {red, blue};
    }
    
}