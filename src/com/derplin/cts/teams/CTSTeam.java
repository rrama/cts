package com.derplin.cts.teams;

import com.derplin.cts.regions.Base;
import com.derplin.cts.regions.SheepPen;
import com.derplin.games.Games;
import com.derplin.games.Util;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;

public class CTSTeam {
    
    private final Team team;
    private final ChatColor colour;
    private final DyeColor dye;
    private final Vector playerSpawn, sheepSpawn;
    private final SheepPen sheepPen;
    private final Base base;
    private int intel = 0;
    
    public CTSTeam(String name, ChatColor colour, DyeColor dye) {
        team = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam(name);
        team.setAllowFriendlyFire(false);
        this.colour = colour;
        team.setPrefix(colour.toString());
        this.dye = dye;
        
        name = name.toLowerCase();
        
        playerSpawn = Util.stringToVector(Games.mapMeta.getString("player-spawn." + name));
        sheepSpawn  = Util.stringToVector(Games.mapMeta.getString("sheep-spawn." + name));
        Vector b1   = Util.stringToVector(Games.mapMeta.getString("sheep-pen." + name + ".b1"));
        Vector b2   = Util.stringToVector(Games.mapMeta.getString("sheep-pen." + name + ".b2"));
        sheepPen = new SheepPen(this, b1, b2);
        int x1 = Games.mapMeta.getInt("base." + name + ".maxX");
        int z1 = Games.mapMeta.getInt("base." + name + ".maxZ");
        int x2 = Games.mapMeta.getInt("base." + name + ".minX");
        int z2 = Games.mapMeta.getInt("base." + name + ".minZ");
        base = new Base(x1, z1, x2, z2);
    }
    
    public Team getTeam() {
        return team;
    }
    
    public ChatColor getColour() {
        return colour;
    }
    
    public String getColouredName() {
        return getColour() + getTeam().getName();
    }
    
    public DyeColor getDye() {
        return dye;
    }
    
    public Vector getPlayerSpawn() {
        return playerSpawn;
    }
    
    public Vector getSheepSpawn() {
        return sheepSpawn;
    }
    
    public SheepPen getSheepPen() {
        return sheepPen;
    }
    
    public Base getBase() {
        return base;
    }
    
    public int getIntel() {
        return intel;
    }
    
    public void increaseIntel() {
        intel++;
    }
    
    public void decreaseIntel() {
        intel--;
    }
    
}