package com.derplin.cts.events;

import com.derplin.cts.CTS;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.SheepDyeWoolEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;

public class SheepSafety implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onSheepDyeWool(SheepDyeWoolEvent event) {
        event.setCancelled(true);
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerShearEntity(PlayerShearEntityEvent event) {
        if (CTS.isTeamSheap(event.getEntity())) {
            event.setCancelled(true);
        }
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamage(EntityDamageEvent event) {
        if (CTS.isTeamSheap(event.getEntity())) {
            event.setCancelled(true);
        }
    }
    
}