package com.derplin.cts.events;

import com.derplin.cts.CTS;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

/**
 * Calls for registering {@link Event}s are in this class.
 */
public class RegisterCTSEvents {
    
    /**
     * Registers all the round {@link Event}s.
     */
    public static void register() {
        reg(new CaptureIntel());
        reg(new Reffing());
        reg(new Respawn());
        reg(new SheepSafety());
    }
    
    /**
     * Registers the {@link Event} {@link Listener}.
     * @param ll the {@link Listener} to register.
     */
    protected static void reg(Listener ll) {
        Bukkit.getPluginManager().registerEvents(ll, CTS.plugin);
    }
    
    /**
     * Unregisters the {@link Event} {@link Listener}.
     * @param ll the {@link Listener} to unregister.
     */
    protected static void unreg(Listener ll) {
        HandlerList.unregisterAll(ll);
    }
}
