package com.derplin.cts.events;

import com.derplin.games.events.extra.RefAddEvent;
import com.derplin.games.events.extra.RefRemoveEvent;
import com.derplin.teamgames.TeamGames;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class Reffing implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onRefAdd(RefAddEvent event) {
        TeamGames.getPlayerTeam(event.getPlayer()).removePlayer(event.getPlayer());
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onRefRemove(RefRemoveEvent event) {
        event.setCancelled(true);
        event.setDisallowMsg(ChatColor.YELLOW + "The round has started, you can not join in as a player now.");
    }
    
}