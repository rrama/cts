package com.derplin.cts.events;

import com.derplin.cts.teams.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class Respawn implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        try {
            event.setRespawnLocation(TeamManager.getCTSTeam(event.getPlayer()).getPlayerSpawn().toLocation(Bukkit.getWorlds().get(0)));
        } catch (Exception ex) {
            try {
                event.setRespawnLocation(Bukkit.getWorlds().get(0).getSpawnLocation());
            } catch (Exception ex2) {
                event.getPlayer().kickPlayer(ChatColor.DARK_RED + "Unable to respawn you properly.");
            }
        }
    }
    
}