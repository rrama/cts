package com.derplin.cts.events;

import com.derplin.cts.teams.TeamManager;
import com.derplin.games.Util;
import com.derplin.teamgames.TeamGames;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Team;

/**
 * Dud class for now.
 */
public class CaptureIntel implements Listener {
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryPickupItem(InventoryPickupItemEvent event) {
        if (event.isCancelled()) return;
        if (!event.getInventory().getTitle().startsWith(ChatColor.GOLD + "INTEL DROP ")) return;
        ItemStack iss = event.getItem().getItemStack();
        if (Util.matchesItemStack(iss, Material.PAPER, ChatColor.COLOR_CHAR + ".+ Team's Intel", "Take this back to", "Jerry in your base.")) {
            event.setCancelled(true);
            return;
        }
        try {
            String intelName = iss.getItemMeta().getDisplayName();
            intelName = intelName.substring(3, intelName.length() - 12);
            String thrower = event.getItem().getMetadata("Thrower").get(0).asString();
            Team team = TeamGames.getPlayerTeam(thrower);
            if (team.getName().equalsIgnoreCase(intelName)) {
                TeamManager.getCTSTeam(team).increaseIntel();
                messageOffline(thrower, ChatColor.GOLD + "Captured the enermy intel!");
            } else {
                messageOffline(thrower, ChatColor.GOLD + "Returned your intel.");
            }
            event.getItem().remove();
        } catch (Exception ex) {}
    }
    
    private static void messageOffline(String pn, String msg) {
        try {
            Bukkit.getOfflinePlayer(pn).getPlayer().sendMessage(msg);
        } catch (Exception ex) {}
    }
    
}