package com.derplin.cts;

import com.derplin.games.GamePlugin;
import com.derplin.games.Games;
import com.derplin.games.gameutil.MapManager;
import com.derplin.games.store.StoreCmd;
import com.derplin.games.store.VaultEconomy;
import com.derplin.roundedgames.Rounded;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

/**
 * The main class for the Capture The Sheep game.
 *
 * @author rrama, Lonesface and Arrem
 *         All rights reserved to rrama, Lonesface and Arrem. Use by Derplin only.
 *         No copying/stealing any part of the code (Exceptions; You have written consent from a member of the Derplin development team).
 *         No copying/stealing ideas from the code (Exceptions; You have written consent from a member of the Derplin development team).<br />
 *         <p/>
 *         Credit goes to rrama (author), Lonesface (author), Arrem (author)
 */
public class CTS extends GamePlugin {

    public static CTS plugin;

    @Override
    public void onDisable() {
        Bukkit.getScheduler().cancelTasks(plugin);
    }
    
    @Override
    public void onLoad() {
        Games.game = this;
        Rounded.roundManager = new Rounds();
        MapManager.initialiseMaps(true);
        Rounded.callAfterLoad();
    }
    
    @Override
    public void onEnable() {
        plugin = this;
        
        Rounded.minimumPlayers = 2;
        Rounded.roundTime = 600;
        
        StoreCmd.eco = new VaultEconomy();
    }
    
    @Override
    public void onMainWorldLoad() {
        Rounded.tryStartGame();
    }
    
    public static boolean isTeamSheap(Entity e) {
        if (e instanceof LivingEntity) {
            String name = ((LivingEntity) e).getCustomName();
            return name != null && name.matches(".+ Team's Sheep");
        }
        return false;
    }
    
}