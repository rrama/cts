package com.derplin.cts;

import com.derplin.cts.events.RegisterCTSEvents;
import com.derplin.cts.teams.CTSTeam;
import com.derplin.cts.teams.TeamManager;
import com.derplin.games.events.XPKill;
import com.derplin.roundedgames.RoundManager;
import com.derplin.roundedgames.events.LateArrivals;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Sheep;

public class Rounds extends RoundManager {

    @Override
    public void roundStart() {
        LateArrivals.register();
        TeamManager.setupTeams();
        RegisterCTSEvents.register();
        for (CTSTeam team : TeamManager.getTeams()) {
            Location loc = team.getSheepSpawn().toLocation(Bukkit.getWorlds().get(0));
            boolean loaded = loc.getChunk().isLoaded();
            if (!loaded) {
                loc.getChunk().load();
            }
            Sheep sheep = (Sheep) Bukkit.getWorlds().get(0).spawnEntity(loc, EntityType.SHEEP);
            sheep.setCustomName(team.getColouredName() + " Team's Sheep");
            sheep.setColor(team.getDye());
            if (!loaded) {
                loc.getChunk().unload();
            }
            for (OfflinePlayer op : team.getTeam().getPlayers()) {
                if (!op.isOnline()) {
                    team.getTeam().removePlayer(op);
                    continue;
                }
                op.getPlayer().teleport(team.getPlayerSpawn().toLocation(Bukkit.getWorlds().get(0)));
            }
        }
        XPKill.register();
    }

    @Override
    public void roundEnd() {}
    
}