# README #

Please note this repository's **code has not been updated since early 2014!**

This repository uses the my newer, but still outdated, Games API found [here](https://bitbucket.org/rrama/games).

## Content ##

In this repository is an abandoned mini-game plugin, like capture the flag, for a game (Minecraft) with;

* In-game commands.
* Handling ingame events (e.g. capturing the sheep).
* Teams
* And not that much more...

## Author ##
[rrama](https://bitbucket.org/rrama/) (Ben Durrans).